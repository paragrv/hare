const fileData = require('../app/models/fileData');


class fileDataCls {
    fileDataCls() { }

    save(params) {
        const fileDocument = new fileData({
            fileName: params.fileName ? params.fileName : null,
            fileData: params.fileData ? params.fileData : null,
            UserName: params.UserName ? params.UserName : null,
            ChannelName: params.channelName ? params.channelName : null,
        });
        return new Promise((resolve, reject) => {
            fileDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo fileDocument Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }


    // Find fileData By File Name 
    findUseractivityByUserName(params) {
        return new Promise((resolve, reject) => {
            fileData.find({ userName: params.userName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

    // Find fileData By File Name 
    findFileDataByFileName(params) {
        return new Promise((resolve, reject) => {
            fileData.findOne({fileName: params.fileName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }


    findall() {
        return new Promise((resolve, reject) => {
            fileData.find({})
                .sort({ created_at: -1 })
                .exec((err, channel) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all channel has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(channel);
                    }
                });
        });
    }

    // Delete Channel
    deleteFile(params) {
        return new Promise((resolve, reject) => {
            const obj = {};
            fileData.deleteOne({ fileName: params.File_name })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Channel SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }

    findFileDataByUserName(params) {
        return new Promise((resolve, reject) => {
            fileData.find({ UserName: params.UserName })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

}

module.exports = {
    fileDataClass: fileDataCls,
};