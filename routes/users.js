var express = require('express');
var router = express.Router();

// ##############################################################################

var fs = require('fs');
const multerS3 = require('multer-s3');
const awsConfig = require('../config/aws_config')['development'];
var multer = require('multer')
var AWS = require('aws-sdk');
var s3 = new AWS.S3();
const { spawn } = require('child_process');

const cognito = require('../app/utilities/user_management/cognito');

const fileData = require("../workers/fileData");
const fileDataObj = new fileData.fileDataClass();

const fileDataEpg = require("../workers/fileDataEpg");
const fileDataEpgObj = new fileDataEpg.fileDataEpgClass();

const airfileData = require("../workers/airFileData");
const airfileDataObj = new airfileData.airfileDataClass();

/* GET users listing. */
router.get('/', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/index', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
});


router.get('/user-groups', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/user-groups', { section: 'Users Listing', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})

router.get('/temp', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('temp', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})


router.get('/user-xlsheetdata', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/user-groups', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})


router.get('/epg_data', function (req, res, next) {
  const clientName = req.session.clientName ? req.session.clientName : null;
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/epg_data', { section: '', sub_section: '', userData: userData, userName: userName, userRole: userRole, clientName: clientName });
})

var EpgfileNameOld = '';
var fileNameInEpg = '';
router.post('/temp', (req, res, next) => {
  let fileNAME = '';

  const upload = multer({
    storage: multerS3({
      s3: s3,
      acl: 'public-read',
      bucket: `${awsConfig.bucket}/${awsConfig.putfolder}`,
      key: (req, file, cb) => {
        console.log(`REAL FILENAME>> ${JSON.stringify(file)}`);
        fileNAME = `${file.originalname}`;
        console.log(`FILE NAME>> ${fileNAME}`);
        cb(null, fileNAME);
      },
    }),
  }).array('userPhoto', 1);

  upload(req, res, (error) => {
    if (error) {
      console.log(`VIDEO UPLOAD ERROR: ${error}`);
    } else {
      console.log('VIDEO UPLOAD DONE!!!!!!');
      res.send("upload")

      fileNameInEpg = fileNAME;

    }
  });
})

router.post('/tempepg', (req, res, next) => {
  let fileNAME1 = '';

  const upload1 = multer({
    storage: multerS3({
      s3: s3,
      acl: 'public-read',
      bucket: `${awsConfig.bucket}/${awsConfig.putfolder1}`,
      key: (req, file, cb) => {
        console.log(`REAL FILENAME>> ${JSON.stringify(file)}`);
        fileNAME1 = `${file.originalname}`;
        console.log(`FILE NAME>> ${fileNAME1}`);
        cb(null, fileNAME1);
      },
    }),
  }).array('videoUpload', 1);

  upload1(req, res, (error) => {
    if (error) {
      console.log(`VIDEO UPLOAD ERROR: ${error}`);
    } else {
      console.log('VIDEO UPLOAD DONE!!!!!!');
      res.send("upload")
      EpgfileNameOld = fileNAME1;
    }
  });
})


router.post('/saveplaylist', (req, res, next) => {

  var fileData = req.body.fileData;
  var fileName = fileNameInEpg;
  var UserName = req.session.clientName;

  const params = {
    fileData: fileData,
    fileName: fileName,
    UserName: UserName,
    channelName : "wellness"
  }
  fileDataObj.save(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: 'Filedata Saved successfuly',
        data: _response,
      })
    } else {
      console.log('Filedata data save failed');
      res.json({
        status: 401,
        message: 'Filedata Save failed',
        data: null,
      })
    }

  })
})

router.post('/saveEpg', (req, res, next) => {

  var fileDataEpg = req.body.fileData1;
  var fileName = fileNameInEpg;
  var UserName = req.session.clientName;
  var EpgfileName = EpgfileNameOld;

  const params = {
    fileDataEpg: fileDataEpg,
    playlistFilename: fileName,
    UserName: UserName,
    EpgfileName: EpgfileName,
    channelName : "wellness"
  }
  fileDataEpgObj.save(params).then((_response) => {
    if (_response) {
      res.json({
        status: 200,
        message: 'Filedata Saved successfuly',
        data: _response,
      })
    } else {
      console.log('Filedata data save failed');
      res.json({
        status: 401,
        message: 'Filedata Save failed',
        data: null,
      })
    }

  })
})

// Get All Users
router.post('/getXslxdata', (req, res, next) => {

  const params = {
    UserName: req.session.clientName
  }
  fileDataObj.findFileDataByUserName(params).then((data) => {
    res.json({
      status: 200,
      message: 'Objects fetched successfully',
      data: data,
    });
  });
});


// registration
router.post('/register', (req, res, next) => {

  const params = {
    name: req.body.name ? req.body.name : null,
    email: req.body.email ? req.body.email : null,
    password: req.body.password ? req.body.password : null
  };

  cognito.RegisterUser(params).then((response) => {
    if (response) {
      res.json({
        status: 200,
        message: 'User Saved Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER SAVE: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
});


// Delete File from s3 then MongoDb
router.post('/deletefile', (req, res, next) => {

  const params = {
    File_name: req.body.File_name ? req.body.File_name : null
  }
  cognito.deleteObject(params).then((data) => {
    if (data) {
      fileDataObj.deleteFile(params).then((data) => {
        res.json({
          status: 200,
          message: 'Delete-channel successfully',
          data: data
        });
      });
    }
  });
});


// // Get file data by filename
router.get('/getFileDatabyFilename', (req, res, next) => {

  const params = {
    fileName: req.query.fileName ? req.query.fileName : null
  }
  fileDataObj.findFileDataByFileName(params).then((data) => {
    res.json({
      status: 200,
      message: 'getFileDatabyFilename fetched successfully',
      data: data
    });
  });
});



// // Get file data by filename
router.get('/getFileDatabyEpgFilename', (req, res, next) => {

  const params = {
    fileName: req.query.fileName ? req.query.fileName : null
  }
  fileDataEpgObj.findFileDataByFileName(params).then((data) => {
    res.json({
      status: 200,
      message: 'getFileDatabyFilename fetched successfully',
      data: data
    });
  });
});


// // Get file data by filename while [.air file generation]
router.post('/getFileDatabyFilenameForOutput', (req, res, next) => {

  const params = {
    fileName: req.body.fileName ? req.body.fileName : null
  }
  fileDataObj.findFileDataByFileName(params).then((data) => {
    res.json({
      status: 200,
      message: 'getFileDatabyFilenameForOutput fetched successfully',
      data: data
    });
  });
});




// insert air file data into db
router.post('/airfileInsert', (req, res, next) => {

  const params = {
    airfileData: req.body.filedata,
    playlistfileName : req.body.PlaylistName,
    channelName : req.body.channelName,
    airfileName : req.body.airfileName,
    UserName : req.session.clientName
  }
  airfileDataObj.save(params).then((data) => {
    res.json({
      status: 200,
      message: 'AIR file Insert successfully',
      data: data,
    });
  });
});

module.exports = router;