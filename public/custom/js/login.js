    // Login
    $("#login-form").submit(function (event) {
        $.post('/login',
            {
                username: $("#email").val().trim().toLowerCase(),
                password: $("#password").val().trim()
            },
            function (data, status) {
                if (data.status === 200) {
                    if (data.data === 'srmd') {
                        window.location.href = '/v1/userSrmd/userSrmd';
                    } else if (data.data === 'wellness') {
                        window.location.href = '/v1/users/temp';
                    } else if (data.data === 'onetake'){
                        window.location.href = '/v1/userOnetake/userOnetake';
                    } else if (data.data === 'harekrishna'){
                        window.location.href = '/v1/userHarekrishna/userHarekrishna';
                    }
                } else {
                    toastr.error(data.message);
                }
            });
        event.preventDefault()
    });
