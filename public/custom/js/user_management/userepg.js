$("#uploadFormEPG").submit(function (event) {

    var formData1 = new FormData();

     formData1.append('userFile', $('#jsonUploadEpg')[0].file[0]);
  
    $.ajax({
        url: '/v1/users/tempepg',
        type: 'POST',
        data: formData1,
        cache: false,
        contentType: false,
        async: false,
        processData: false
    }).then(function (data) {
        if (data.status === 200) {
            setTimeout(function () {
                toastr.success('File Uploaded Successfully IN js file.');
            }, 3000);

            params['fileName'] = data.data.fileName;

        } else {
            console.log("Oops! JSON Upload ERROR >  " + data.message)
        }
    })

    event.preventDefault()
})

