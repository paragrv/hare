var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;

$.get(`/v1/userOnetake/getFileDatabyFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {
            if (data === null) {
                var _data = data.data
            } else {
                var GetOnetakefileData = data.data
                appendFiledatabyfileNamewithOnetake(GetOnetakefileData)
            }
        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })

function appendFiledatabyfileNamewithOnetake(GetOnetakefileData) {
    if (GetOnetakefileData !== null) {

        var array = GetOnetakefileData.fileData
        var options_table = "";

        array.forEach(function (element, i) {

            var Name = element.Name ? element.Name : "-";
            var StartDate = element.StartDate ? element.StartDate : "-";
            var ContentID = element.ContentID ? element.ContentID : "-";
            var ProgramName = element.ProgramName ? element.ProgramName : "-";
            var ProgramFile = element.ProgramFile ? element.ProgramFile : "-";
            var OnAirTime = element.OnAirTime ? element.OnAirTime : "-";
            var DURATION = element.Duration ? element.Duration : "-";

            options_table += `<tr class="users-tbl-row asset-row">
                       <td class="Channel">${Name}</td>
                       <td class="name">${StartDate}</td>
                       <td class="name">${ContentID}</td>
                       <td class="name">${ProgramName}</td>
                       <td class="name">${ProgramFile}</td>
                       <td class="name">${OnAirTime}</td>
                       <td class="name">${DURATION}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#onetakexldata_tbody').append(options_table);
            }
        })
    }
}


$(document).ready(function () {

    $("#uploadFormOnetake").submit(function (event) {

        var formData = new FormData();
        formData.append('userOnetake', $('#userOnetake')[0].files[0]);
        formData.append('channel_name', $('option:selected', this).text() ? $('option:selected', this).text() : null);
        $.ajax({
            url: '/v1/userOnetake/fileOnetake',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {
            if (data.status === 200 || data) {

                var fileUpload = document.getElementById("userOnetake");

                //Validate whether File is valid Excel file.
                if (fileUpload.value.toLowerCase()) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();

                        //For Browsers other than IE.
                        if (reader.readAsBinaryString) {
                            reader.onload = function (e) {
                                ProcessExcel(e.target.result);
                            };
                            reader.readAsBinaryString(fileUpload.files[0]);
                        } else {
                            //For IE Browser.
                            reader.onload = function (e) {
                                var data = "";
                                var bytes = new Uint8Array(e.target.result);
                                for (var i = 0; i < bytes.byteLength; i++) {
                                    data += String.fromCharCode(bytes[i]);
                                }
                                ProcessExcel(data);
                            };
                            reader.readAsArrayBuffer(fileUpload.files[0]);
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                    }
                } else {
                    alert("Please upload a valid Excel file.");
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })


        var formData1 = new FormData();
        formData1.append('userOnetakeEpg', $('#userOnetakeEpg')[0].files[0]);
        $.ajax({
            url: '/v1/userOnetake/fileOnetakeEpg',
            type: 'POST',
            data: formData1,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {
            if (data.status === 200 || data) {
                var fileUpload1 = document.getElementById("userOnetakeEpg");

                //Validate whether File is valid Excel file.
                if (fileUpload1.value.toLowerCase()) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();

                        //For Browsers other than IE.
                        if (reader.readAsBinaryString) {
                            reader.onload = function (e) {
                                ProcessExcel1(e.target.result);
                            };
                            reader.readAsBinaryString(fileUpload1.files[0]);
                        } else {
                            //For IE Browser.
                            reader.onload = function (e) {
                                var data = "";
                                var bytes = new Uint8Array(e.target.result);
                                for (var i = 0; i < bytes.byteLength; i++) {
                                    data += String.fromCharCode(bytes[i]);
                                }
                                ProcessExcel1(data);
                            };
                            reader.readAsArrayBuffer(fileUpload1.files[0]);
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                    }
                } else {
                    alert("Please upload a valid Excel file.");
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })
        event.preventDefault()
    })


    var userChannelName = '';
    var userPlaylistName = '';
    function ProcessExcel(data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        var params = {
            fileData: excelRows
        }
        $.post('/v1/userOnetake/saveplaylist', params, function (data, status) {
            if (data.status == 200) {
                toastr.success('File Uploaded Successfully');
                userChannelName = data.data.message.ChannelName
                userPlaylistName = data.data.message.fileName
                var jsonData = data.data.message.fileData
                functionFormatOnetake(jsonData)
            }
        })
    }


    function functionFormatOnetake(jsonData) {

        var array = jsonData;
        var channelName = userChannelName;
        var PlaylistName = userPlaylistName;
        newSting2 = PlaylistName.replace('.xlsx', '.air')
        var airfileName = newSting2;

        var options_table = [];
        array.forEach(function (element, i) {
            var Name = element.Name ? element.Name : "-";
            var ContentID = element.ContentID ? element.ContentID : "-";
            var ProgramName = element.ProgramName ? element.ProgramName : "-";
            var ProgramFile = element.ProgramFile ? element.ProgramFile : "-";
            var Type = element.Type ? element.Type : "-";

            if (channelName === 'Kids Toons') {

                options_table.push(`movie 00:00:00.00 X:\\KIDS TOONS_NEW\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Cooking') {

                options_table.push(`movie 00:00:00.00 X:\\COOKING_NEW\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Kids Rhymes') {

                options_table.push(`movie 00:00:00.00  X:\\KIDS RHYMES_NEW\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Hollywood Action') {

                options_table.push(`comment 0 # X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   X:\\HOLLYWOOD ACTION_NEW\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Bhojpuri Songs') {

                options_table.push(`movie 00:00:00.00 Y:\\BHOJPURI_NEW\\SONGS\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Bengali Movies') {

                options_table.push(`comment 0 # X:\\BENGALI_NEW\\${ContentID}_${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   X:\\BENGALI_NEW\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Bhojpuri Movies') {

                options_table.push(`movie 00:00:00.00 X:\\BHOJPURI_NEW\\${Type}\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Hollywood Gujrati Movies') {

                options_table.push(`comment 0 # S:\\GUJARATI MOVIES_NEW\\Movies\\${ContentID}_${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   S:\\GUJARATI MOVIES_NEW\\Movies\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Hollywood Marathi Movies') {

                options_table.push(`comment 0 # S:\\MARATHI MOVIE_NEW\\Movies\\${ContentID}_${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   S:\\MARATHI MOVIE_NEW\\Movies\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Hollywood Movies') {

                options_table.push(`comment 0 # Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   Y:\\KCCL_HOLLYWOOD ONE_NEW\\${ProgramFile}`)

            } else if (channelName === 'Hollywood Tamil') {

                options_table.push(`comment 0 # Y:\\TAMIL_NEW\\MOVIES\\${ContentID}_${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   Y:\\TAMIL_NEW\\MOVIES\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Hollywood Telugu') {

                options_table.push(`comment 0 # Y:\\TELUGU_NEW\\${ContentID}_${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   Y:\\TELUGU_NEW\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'K-PoP') {

                options_table.push(`comment 0 # Y:\\KOREAN_NEW\\K-POP\\${ContentID}_${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   Y:\\KOREAN_NEW\\K-POP\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'K-World') {

                options_table.push(`comment 0 # X:\\KOREAN_NEW\\CONTENT\\${ContentID}_${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   X:\\KOREAN_NEW\\CONTENT\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Kids Movies') {

                options_table.push(`movie 00:00:00.00 X:\\KIDS MOVIES_NEW\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'Kids Toons Marathi') {

                options_table.push(`movie 00:00:00.00 S:\\KIDS TOONS MARATHI_NEW\\${ProgramFile}`)

            } else if (channelName === 'Malayalam') {

                options_table.push(`comment 0 # X:\\MALYALAM_NEW\\${ContentID}_${ProgramFile}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00  X:\\MALYALAM_NEW\\${ContentID}_${ProgramFile}`)

            } else if (channelName === 'SongDew') {

                options_table.push(``)

            } else if (channelName === 'South Action') {

                options_table.push(`comment 0 # X:\\SOUTH ACTION_NEW\\${ContentID}_${ProgramFileName}|0:00:0.00|<ST 0>${ProgramName}`)
                options_table.push(`movie 00:00:00:00   X:\\SOUTH ACTION_NEW\\${ContentID}_${ProgramFileName}`)

            }

            if (i == array.length - 1) {

                //initiate for 1st row
                var filedata = options_table;
                let params = {
                    filedata: filedata,
                    channelName: channelName,
                    PlaylistName: PlaylistName,
                    airfileName: airfileName
                }
                $.post('/v1/userOnetake/airfileInsert', params,
                    function (data, textStatus, request) {
                        if (data.status === 200) {

                            setTimeout(function () {
                                window.location.href = '/v1/userOnetake/userOnetake'
                            }, 2000);

                        } else {
                            toastr.danger('Creation failed.')
                        }
                    })
                var text = filedata;
            }
        })
    }


    function ProcessExcel1(data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];
        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        var params = {
            fileData1: excelRows
        }

        $.post('/v1/userOnetake/saveEpg', params, function (data, status) {
            if (data.status == 200) {
                
            }
        })

    }

    $.post('/v1/userOnetake/datatodisplay', function (data, status) {
        if (data.status == 200) {
            destroyRowsxllist()
            appendUploadedsData(data)
        }
    });

    function destroyRowsxllist() {
        $('#xllist_tbody').empty()
        $('#xllist_table').DataTable().rows().remove();
        $("#xllist_table").DataTable().destroy()
    }


    function appendUploadedsData(data) {
        var array = data.data;
        if (array.length) {
            var uploaded_list = "";
            array.forEach(function (element, i) {

                var fileName = element.fileName ? element.fileName : "";
                var created_at = element.created_at ? moment(element.created_at).format('lll') : "";

                uploaded_list += `<tr class="users-tbl-row asset-row" id="${fileName}">
            <td class="">${(i + 1)}</td>
              <td class="username" key_factor="${fileName}"><a class="group-name-link"  href="/v1/userOnetake/userOnetake-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
            <td class="name">${created_at}</td>
            <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
            <a href="#" key-value="${fileName}" class="dropdown-item delete-file">Delete</a> 
            <a href="/v1/userOnetake/epg_Onetake?fileName=${fileName}" key-value="${fileName}" class="dropdown-item">View Epg</a> </div> </div></div> </div></td>`;

                if (i == array.length - 1) {
                    $('#xllist_tbody').append(uploaded_list);
                    reInitialiClientSheetListTable()
                }
            })
        } else {

        }
    }


    function reInitialiClientSheetListTable() {
        $("#xllist_table").DataTable().destroy()
        xllist_table = $('#xllist_table').DataTable({
            //"order": [[1, "desc"]], // for descending order
            "columnDefs": [
                { "width": "30%", "targets": 1 }
            ]
        })
        $("#xllist_table tbody tr:first").addClass("active");
    }


    // Delete file
    $(document).on("click", ".delete-file", function (event) {

        var File_name = $(this).attr('key-value');
        let params = {
            File_name: File_name
        }

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1
        }).then(function (t) {
            if (t.value) {

                $.post('/v1/users/deletefile', params, function (data, status) {
                    if (data.status == 200) {
                        toastr.success('File Deleted');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000)
                        event.preventDefault()
                    }
                });
            }
        })
    })


    $.get(`/v1/userOnetake/getFileDatabyEpgFilename?fileName=${fileName}`,
        function (data, status) {
            if (data.status === 200) {

                if (data === null) {
                    var data1 = data.data
                } else {
                    var GetOnetakefileDataEpg = data.data
                    appendFiledataOnetakeEpgfileName(GetOnetakefileDataEpg)
                }
            } else {
                console.log("get getFileDatabyFilename Failed")
            }
        })


    function appendFiledataOnetakeEpgfileName(GetOnetakefileDataEpg) {
        if (GetOnetakefileDataEpg !== null) {
            var array = GetOnetakefileDataEpg.fileData;
            var options_table = "";

            array.forEach(function (element, i) {

                var Date = element.Date ? element.Date : "-";
                var Time = element.Time ? element.Time : "-";
                var Duration = element.Duration ? element.Duration : "-";
                var Title = element.Title ? element.Title : "-";
                var Synopsis = element.Synopsis ? element.Synopsis : "-";
                var Genure = element.Genre ? element.Genre : "-";
                var Sub_Genure = element.SubGenre ? element.SubGenre : "-";
                var Language = element.Language ? element.Language : "-";

                options_table += `<tr class="users-tbl-row asset-row">
                       <td class="date">${Date}</td>
                       <td class="name">${Time}</td>
                       <td class="name">${Duration}</td>
                       <td class="name">${Title}</td>
                       <td class="name" >${Synopsis}</td>
                       <td class="name">${Genure}</td>
                       <td class="text-break">${Sub_Genure}</td>
                       <td class="name">${Language}</td>`;

                if (i == array.length - 1) {
                    //initiate for 1st row
                    $('#epgOnetakedata_tbody').append(options_table);
                }
            })
        }
    }
})


