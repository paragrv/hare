var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;

$.get(`/v1/users/getFileDatabyFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {
            if (data === null) {
                var _data = data.data
            } else {
                var GetfileData = data.data;
                appendFiledatabyfileName(GetfileData)
            }

        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })


function appendFiledatabyfileName(GetfileData) {
    if (GetfileData !== null) {

        var array = GetfileData.fileData;
      
        var options_table = "";

        array.forEach(function (element, i) {

            var SrNo = element.SrNo ? element.SrNo : "-";
            var ProgrammeName = element.ProgrammeName ? element.ProgrammeName : "-";
            var TOPIC = element.TOPIC ? element.TOPIC : "-";
            var Subject = element.Subject ? element.Subject : "-";
            var CodeNo = element.CodeNo ? element.CodeNo : "-";
            var Duration = element.Duration ? element.Duration : "-";

            options_table += `<tr class="users-tbl-row asset-row">
                   <td class="${(i + 1)}">${SrNo}</td>
                   <td class="name">${ProgrammeName}</td>
                   <td class="name">${TOPIC}</td>
                   <td class="name">${Subject}</td>
                   <td class="name">${CodeNo}</td>
                   <td class="name">${Duration}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#xldata_tbody').append(options_table);

            }
        })
    }
}


$(document).ready(function () {

    $("#uploadForm").submit(function (event) {
        var formData = new FormData();
        formData.append('userPhoto', $('#userPhoto')[0].files[0]);
        $.ajax({
            url: '/v1/users/temp',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {

            if (data.status === 200 || data) {

                var fileUpload = document.getElementById("userPhoto");
                //Validate whether File is valid Excel file.
                if (fileUpload.value.toLowerCase()) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();

                        //For Browsers other than IE.
                        if (reader.readAsBinaryString) {
                            reader.onload = function (e) {
                                ProcessExcel(e.target.result);
                            };
                            reader.readAsBinaryString(fileUpload.files[0]);
                        } else {
                            //For IE Browser.
                            reader.onload = function (e) {
                                var data = "";
                                var bytes = new Uint8Array(e.target.result);
                                for (var i = 0; i < bytes.byteLength; i++) {
                                    data += String.fromCharCode(bytes[i]);
                                }
                                ProcessExcel(data);
                            };
                            reader.readAsArrayBuffer(fileUpload.files[0]);
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                    }
                } else {
                    alert("Please upload a valid Excel file.");
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })

        var formData1 = new FormData();
        formData1.append('videoUpload', $('#input1')[0].files[0]);
        $.ajax({
            url: '/v1/users/tempepg',
            type: 'POST',
            data: formData1,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {
            if (data.status === 200 || data) {

                var fileUpload1 = document.getElementById("input1");

                //Validate whether File is valid Excel file.
                if (fileUpload1.value.toLowerCase()) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();

                        //For Browsers other than IE.
                        if (reader.readAsBinaryString) {
                            reader.onload = function (e) {
                                ProcessExcel1(e.target.result);
                            };
                            reader.readAsBinaryString(fileUpload1.files[0]);
                        } else {
                            //For IE Browser.
                            reader.onload = function (e) {
                                var data = "";
                                var bytes = new Uint8Array(e.target.result);
                                for (var i = 0; i < bytes.byteLength; i++) {
                                    data += String.fromCharCode(bytes[i]);
                                }
                                ProcessExcel1(data);
                            };
                            reader.readAsArrayBuffer(fileUpload1.files[0]);
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                    }
                } else {
                    alert("Please upload a valid Excel file.");
                }

            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })
        event.preventDefault()
    })


    var userChannelName = '';
    var userPlaylistName = '';
    function ProcessExcel(data) {
      
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];
        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        var params = {
            fileData: excelRows
        }
        $.post('/v1/users/saveplaylist', params, function (data, status) {
            if (data.status == 200) {
                userChannelName = data.data.message.ChannelName
                userPlaylistName = data.data.message.fileName
                var jsonData = data.data.message.fileData
                functionFormatWellness(jsonData)
                toastr.success('File Uploaded Successfully');
            }
        })

    }



    function functionFormatWellness(jsonData){
        
        var array = jsonData;
        var channelName = userChannelName;
        var PlaylistName = userPlaylistName;
        newSting2 = PlaylistName.replace('.xlsx', '.air')
        var airfileName = newSting2;
       
        var options_table = [];
        array.forEach(function (element, i) {
            var SrNo = element.SrNo ? element.SrNo : "";
            var ProgrammeName = element.ProgrammeName ? element.ProgrammeName : "";
            var TOPIC = element.TOPIC ? element.TOPIC : "";
            var Subject = element.Subject ? element.Subject : "";
            var CodeNo = element.CodeNo ? element.CodeNo : "";
            var Duration = element.Duration ? element.Duration : "";
    
            options_table.push(`movie 0:00:00.0 U:\\Content\\${CodeNo}.mp4`)
    
            if (i == array.length - 1) {
                 
                //initiate for 1st row
                var filedata = options_table;
                let params = {
                    filedata : filedata,
                    channelName : channelName,
                    PlaylistName : PlaylistName,
                    airfileName : airfileName
                }
                $.post('/v1/users/airfileInsert', params,
                function (data, textStatus, request) {
                    if (data.status === 200) {
                      
                        setTimeout(function () {
                            window.location.href = '/v1/users/temp'
                        }, 2000);
    
                    } else {
                        toastr.danger('Creation failed.')
                    }
                })
                var text = filedata;
            }
        })
    
    }


    function ProcessExcel1(data) {
      
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });
        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];
        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        var params = {
            fileData1: excelRows
        }
        $.post('/v1/users/saveEpg', params, function (data, status) {
            if (data.status == 200) {
            }
        })
    }


    // getUsers
    $.post('/v1/users/getXslxdata', function (data, status) {
        if (data.status == 200) {
            destroyRowsxllist()
            appendUploadedsData(data)
        }
    });
    function destroyRowsxllist() {
        $('#xllist_tbody').empty()
        $('#xllist_table').DataTable().rows().remove();
        $("#xllist_table").DataTable().destroy()
    }

    function appendUploadedsData(data) {
        var array = data.data;
        if (array.length) {
            var uploaded_list = "";
            array.forEach(function (element, i) {

                var fileName = element.fileName ? element.fileName : "";
                var created_at = element.created_at ? moment(element.created_at).format('lll') : "";

                uploaded_list += `<tr class="users-tbl-row asset-row" id="${fileName}">
            <td class="">${(i + 1)}</td>
            <td class="username " key_factor="${fileName}"><a class="group-name-link"  href="/v1/users/user-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
            <td class="name">${created_at}</td>
            <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
            <a href="#" key-value="${fileName}" class="dropdown-item delete-file">Delete</a> 
            <a href="/v1/users/epg_data?fileName=${fileName}" key-value="${fileName}" class="dropdown-item">View Epg</a> </div> </div>
            </td>`
                if (i == array.length - 1) {
                    $('#xllist_tbody').append(uploaded_list)
                    reInitialiClientSheetListTable()
                }
            })
        } 
    }


    function reInitialiClientSheetListTable() {
        $("#xllist_table").DataTable().destroy()
        xllist_table = $('#xllist_table').DataTable({
            //"order": [[1, "desc"]], // for descending order
            "columnDefs": [
                { "width": "30%", "targets": 1 }
            ]
        })
        $("#xllist_table tbody tr:first").addClass("active");
    }


    // Delete file
    $(document).on("click", ".delete-file", function (event) {

        var File_name = $(this).attr('key-value');
        let params = {
            File_name: File_name
        }

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1
        }).then(function (t) {
            if (t.value) {

                $.post('/v1/users/deletefile', params, function (data, status) {

                    if (data.status == 200) {
                        toastr.success('File Deleted');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000)
                        event.preventDefault()
                    }
                });
            }
        })
    })
})


$.get(`/v1/users/getFileDatabyEpgFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {
            if (data === null) {
                var _data = data.data
            } else {
                var GetfileDataEpg = data.data
                appendFiledatabyEpgfileName(GetfileDataEpg)
            }
        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })


function appendFiledatabyEpgfileName(GetfileDataEpg) {
    if (GetfileDataEpg !== null) {

        var array = GetfileDataEpg.fileData
        var options_table = "";

        array.forEach(function (element, i) {

            var Date = element.Date ? element.Date : "-";
            var Start_Time = element.StartTime ? element.StartTime : "-";
            var Stop_Time = element.StopTime ? element.StopTime: "-";
            var Duration = element.Duration ? element.Duration : "-";
            var Title = element.Title ? element.Title : "-";
            var Description = element.Description ? element.Description : "-";
            var Isrepeat = element.Isrepeat ? element.Isrepeat : "-";
            var Thumbnail = element.Thumbnail ? element.Thumbnail : "-";
            var Program_Number = element.ProgramNumber ? element.ProgramNumber : "-";

            options_table += `<tr class="users-tbl-row asset-row">
                   <td class="index">${(i + 1)}</td>
                   <td class="name">${Date}</td>
                   <td class="name">${Start_Time}</td>
                   <td class="name">${Stop_Time}</td>
                   <td class="name">${Duration}</td>
                   <td class="name">${Title}</td>
                   <td class="name">${Description}</td>
                   <td class="name">${Isrepeat}</td>
                   <td class="name">${Thumbnail}</td>
                   <td class="name">${Program_Number}</td>`;

            if (i == array.length - 1) {
                //initiate for 1st row
                $('#epgdata_tbody').append(options_table);
            }
        })
    }
}












