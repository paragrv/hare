
var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var fileName = query('fileName') ? query('fileName') : null;

$.get(`/v1/userHarekrishna/getFileDatabyFilename?fileName=${fileName}`,
    function (data, status) {
        if (data.status === 200) {
            if (data === null) {
                var _data = data.data;
            } else {
                var GetHarefileData = data.data;
                appendFiledatabyfileNamewithHare(GetHarefileData)
            }
        } else {
            console.log("get getFileDatabyFilename Failed")
        }
    })


function appendFiledatabyfileNamewithHare(GetHarefileData) {
    if (GetHarefileData !== null) {

        var array = GetHarefileData.fileData;
        var options_table = "";

        array.forEach(function (element, i) {

            var start_time = element["AiringStartTime"] ? element["AiringStartTime"] : "-";
            var end_time = element["EndTime"] ? element["EndTime"] : "-";
            var ProgramName = element["Program Name "] ? element["Program Name "] : "-";
            var FileName = element["FileName"] ? element["FileName"] : "-";
            var FillersSerialNumber = element["FillersSerialNumber"] ? element["FillersSerialNumber"] : "-"

            //   var array1 = FillersSerialNumber.split(',');
            //   console.log(JSON.stringify(array1))

            options_table += `<tr class="users-tbl-row asset-row">
                           <td class="Channel" style="width:10%">${start_time}</td>
                           <td class="name">${end_time}</td>
                           <td class="name" style="width:20%">${ProgramName}</td>
                           <td class="name" style="width:29%">${FileName}</td>
                           <td class="name">${FillersSerialNumber}</td>`;
            if (i == array.length - 1) {
                //initiate for 1st row
                $('#harexldata_tbody').append(options_table);
            }
        })
    }

}



$(document).ready(function () {

    $("#uploadFormHare").submit(function (event) {
        var formData = new FormData();
        formData.append('userHarekrishna', $('#userHarekrishna')[0].files[0]);
        formData.append('channel_name', $('option:selected', this).text() ? $('option:selected', this).text() : null);
        $.ajax({
            url: '/v1/userHarekrishna/fileHare',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            async: false,
            processData: false
        }).then(function (data) {

            if (data.status === 200 || data) {

                var fileUpload1 = document.getElementById("userHarekrishna");
                //Validate whether File is valid Excel file.
                if (fileUpload1.value.toLowerCase()) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();

                        //For Browsers other than IE.
                        if (reader.readAsBinaryString) {
                            reader.onload = function (e) {
                                ProcessExcel(e.target.result);
                            };
                            reader.readAsBinaryString(fileUpload1.files[0]);
                        } else {
                            //For IE Browser.
                            reader.onload = function (e) {
                                var data = "";
                                var bytes = new Uint8Array(e.target.result);
                                for (var i = 0; i < bytes.byteLength; i++) {
                                    data += String.fromCharCode(bytes[i]);
                                }
                                ProcessExcel(data);
                            };
                            reader.readAsArrayBuffer(fileUpload1.files[0]);
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                    }
                } else {
                    alert("Please upload a valid Excel file.");
                }
            } else {
                console.log("Oops! JSON Upload ERROR >  " + data.message)
            }
        })
        event.preventDefault()
    })


    var userChannelName = '';
    var userPlaylistName = '';
    function ProcessExcel(data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        var params = {
            fileData: excelRows
        }
        $.post('/v1/userHarekrishna/saveplaylist', params, function (data, status) {
            if (data.status == 200) {

                var _data = data.data;
                //  hare(_data)
                fillers(_data)
                // toastr.success('File Uploaded Successfully');
                // setTimeout(function () {
                //     window.location.href = '/v1/userHarekrishna/userHarekrishna'
                // }, 2000);
            }
        })
    }



    function filename(jio) {



        // console.log("fillersparams >> " + JSON.stringify(fillersparams))
        return new Promise((resolve, reject) => {
            $.post('/v1/userHarekrishna/getharekrishna', jio, function (data, status) {
                if (data.status == 200) {
                    console.log("fillers >> "+JSON.stringify(data))
                    var filenameFillers11 = data.data[0].fileData[0].FileNamewithID ? data.data[0].fileData[0].FileNamewithID : "-";

                    // fillerNew = filenameFillers11;
                    //  console.log("fillerNew" + fillerNew)
                    // resolve(fillerNew)
                    resolve(filenameFillers11)
                }
            });
        })

    }

    function fillervalue(fillersparams) {
        // console.log("fillersparams >> " + JSON.stringify(fillersparams))
        return new Promise((resolve, reject) => {
            $.post('/v1/userHarekrishna/getharekrishna', fillersparams, function (data, status) {
                if (data.status == 200) {
                    //console.log("fillers >> "+JSON.stringify(data))
                    var filenameFillers = data.data[0].fileData[0].FileNamewithID ? data.data[0].fileData[0].FileNamewithID : "-";

                    //  filenamewithextension = filenameFillers;

                    // resolve(filenamewithextension)
                    resolve(filenameFillers)

                }
            });
        })
    }



    let filenamewithextension = "";

    var arreayEmpty = [];
    var arrayFillers = [];

    let fillerOne = "";


    let fillerOne2 = "";
    let fillerTwo2 = "";


    let fillerOne3 = "";
    let fillerTwo3 = "";
    let fillerThree3 = "";


    let fillerOne4 = "";
    let fillerTwo4 = "";
    let fillerThree4 = "";
    let fillerFour4 = "";


    let fillerOne5 = "";
    let fillerTwo5 = "";
    let fillerThree5 = "";
    let fillerFour5 = "";
    let fillerFive5 = "";

    async function fillers(_data) {
        console.log("Fillers data >>>>>>>> " + JSON.stringify(_data))

        var jsonData = _data.message.fileData;
        console.log("show jsonData >>>>>>>>>> " + JSON.stringify(jsonData))
        var array = jsonData;
        console.log("Array Length Count >>>>>>>>>>>> " + JSON.stringify(array.length))
        var options_table = [];
        var str = ``;

        for (let element of array) {
            //array.forEach(async function (element, i) {
            var ProgramName = element["ProgramName"] ? element["ProgramName"] : "-";
            var FillersSerialNumber = element["FillersSerialNumber"] ? element["FillersSerialNumber"] : "-";
            var FileName = element["FileName"] ? element["FileName"] : "-";
            console.log(JSON.stringify("FileName >>>>>>>>>>>>>>>>>>>>>" + FileName))
            var fillers = FillersSerialNumber.split(',') ? FillersSerialNumber.split(',') : "-";
            console.log(JSON.stringify("Fillers value >>>>>>>>>>>>>>>>>>" + fillers))


            var jio = {
                File_Name: FileName.trim()
            }

            const promise1 = await filename(jio);

            

            str += `<comment title="# V:\Content\"${promise1}|0:00:0.00|&lt;ST 0&gt;${ProgramName}"/>`

            console.log('Filename Response =>>>>>>>>>>>>>>>>>>' + JSON.stringify(promise1))
            filenamewithextension = promise1
            // alert('promise1 value => response for =>'+JSON.stringify(promise1))


            var fillers_response = [];

            if (fillers !== "-") {
                for (let filler of fillers) {

                    //fillerOne += fillers[j].trim()
                    var fillersparams = {
                        File_Name: filler.trim()
                    }
                    // console.log("fillersparams =>"+fillersparams.File_Name)

                    const promise2 = await fillervalue(fillersparams);
                    str += `<movie file="V:\Content\"${promise2}"/>`
                    fillers_response.push(promise2)
                   



                }


            }


            console.log('Filename >>>>>>>>>>>>' + JSON.stringify(promise1))

            console.log('Fillers Response >>>>>>>>>>>>>>>>>>>>>>>>>>' + JSON.stringify(fillers_response))


            console.log("Final Data >>>>>>>>>>>>>>>" + str)
            // if (i == array.length - 1) {
            //     alert("for loop termination")
            //     console.log("Final Data >>>>>>>>>>>>>>>" + str)

            // }

        }



    }





















    // function hare(_data) {
    //     //console.log("_data >> " + JSON.stringify(_data))

    //     var jsonData = _data.message.fileData;
    //     // console.log("jsonData >> " + JSON.stringify(jsonData))
    //     var array = jsonData;
    //     //console.log("jsonData >> " + JSON.stringify(array.length))
    //     for (var i = 0; i < array.length; i++) {

    //         var File_Name = array[i].FileName ? array[i].FileName : "-";

    //         console.log(File_Name)

    //         if (File_Name !== "-") {

    //             var params = {
    //                 File_Name: File_Name
    //             }

    //             $.post('/v1/userHarekrishna/getharekrishna', params, function (data, status) {
    //                 if (data.status == 200) {
    //                     // console.log("getharekrishna >> "+JSON.stringify(data))

    //                     var filenameid = data.data[0].fileData[0].FileNamewithID ? data.data[0].fileData[0].FileNamewithID : "-";
    //                     // console.log("filenameid >> "+JSON.stringify(filenameid))
    //                     filenamewithextension = filenameid;
    //                     //   console.log("filenamewithextension1 >> "+JSON.stringify(filenamewithextension))

    //                     fileNameTo.push(filenamewithextension)
    //                 }
    //             });
    //         }

    //     }
    // }

    // function functionFormatSrmd(jsonData) {

    //     var array = jsonData;
    //     var channelName = userChannelName;
    //     var PlaylistName = userPlaylistName;
    //     newSting2 = PlaylistName.replace('.xlsx', '.air')
    //     var airfileName = newSting2;

    //     var options_table = [];
    //     array.forEach(function (element, i) {
    //         var Channel = element.Channel ? element.Channel : "-";
    //         var DATE = element.DATE ? element.DATE : "-";
    //         var EventID = element.EventID ? element.EventID : "-";
    //         var TITLE = element.TITLE ? element.TITLE : "-";
    //         var DURATION = element.DURATION ? element.DURATION : "-";

    //         options_table.push(`movie 0:00:00.0 W:\\Raw_Content\\Content\\${EventID}.mp4`)


    //         if (i == array.length - 1) {

    //             //initiate for 1st row
    //             var filedata = options_table
    //             let params = {
    //                 filedata: filedata,
    //                 channelName: channelName,
    //                 PlaylistName: PlaylistName,
    //                 airfileName: airfileName
    //             }

    //             $.post('/v1/userSrmd/airfileInsert', params,
    //                 function (data, textStatus, request) {
    //                     if (data.status === 200) {

    //                         setTimeout(function () {
    //                             window.location.href = '/v1/userSrmd/userSrmd'
    //                         }, 2000);

    //                     } else {
    //                         toastr.danger('Creation failed.')
    //                     }
    //                 })
    //             var text = filedata;
    //         }
    //     })
    // }


    // getXslxdata
    $.get('/v1/userHarekrishna/getXslxdata', function (data, status) {
        if (data.status == 200) {
            destroyRowsxllist()
            appendUploadedsData(data)
        }
    });

    function destroyRowsxllist() {
        $('#harexllist_tbody').empty()
        $('#harexllist_table').DataTable().rows().remove();
        $("#harexllist_table").DataTable().destroy()
    }

    function appendUploadedsData(data) {
        var array = data.data;
        if (array.length) {
            var uploaded_list = "";
            array.forEach(function (element, i) {
                var fileName = element.fileName ? element.fileName : "";
                var created_at = element.created_at ? moment(element.created_at).format('lll') : "";

                uploaded_list += `<tr class="users-tbl-row asset-row" id="${fileName}">
            <td class="">${(i + 1)}</td>
            <td class="username" key_factor="${fileName}"><a class="group-name-link"  href="/v1/userHarekrishna/userHarekrishan-xlsheetdata?fileName=${fileName}">${fileName}</a></td>
            <td class="name">${created_at}</td>
            <td class="action-td" id=${fileName}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
            <a href="#" key-value="${fileName}" class="dropdown-item delete-file">Delete</a> 
            </td>`
                if (i == array.length - 1) {
                    $('#harexllist_tbody').append(uploaded_list);
                    reInitialiClientSheetListTable()
                }
            })
        }
    }


    function reInitialiClientSheetListTable() {
        $("#harexllist_table").DataTable().destroy()
        xllist_table = $('#harexllist_table').DataTable({
            //"order": [[1, "desc"]], // for descending order
            "columnDefs": [
                { "width": "30%", "targets": 1 }
            ]
        })
        $("#harexllist_table tbody tr:first").addClass("active");
    }


    // Delete file
    $(document).on("click", ".delete-file", function (event) {

        var File_name = $(this).attr('key-value');
        let params = {
            File_name: File_name
        }

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1
        }).then(function (t) {
            if (t.value) {
                $.post('/v1/userHarekrishna/deletefile', params, function (data, status) {
                    if (data.status == 200) {
                        toastr.success('File Deleted');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000)
                        event.preventDefault()
                    }
                });
            }
        })
    })
})

