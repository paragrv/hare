const mongoose = require('mongoose');

const fileDataInsertionSchema = mongoose.Schema({

    fileName: { type: String, required: true },
    fileData: { type: Array, required: true },
    UserName: {type: String, required: true},
    ChannelName: {type: String},
    playlistFilename: {type: String},
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

fileDataInsertionSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});

module.exports = mongoose.model('userFileDataEpg', fileDataInsertionSchema, 'userFileDataEpg');